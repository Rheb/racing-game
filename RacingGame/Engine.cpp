// Engine.cpp

#include "stdafx.h"
#include "InputSystem.h"
#include "RenderSystem.h"
#include "Engine.h"

Engine::Engine() {
	m_input_system = nullptr;
	m_render_system = nullptr;
};

Engine::~Engine() {
};

bool Engine::Initialize(Config& config, void* window) {
	HWND hWnd = (HWND)window;
	int width = config.Get("width", 1024);
	int height = config.Get("height", 640);

	try {
		m_render_system = new RenderSystem(hWnd, width, height);
	}
	catch(std::exception e) {
		Debug::Msg(e.what());
		return false;
	};

	m_input_system = new InputSystem;

	// shader
	char vs[1024], ps[1024];
	unsigned int vs_size = File::Size("../data/shader/dx/first_vs.txt");
	unsigned int ps_size = File::Size("../data/shader/dx/first_ps.txt");
	File::Read("../data/shader/dx/first_vs.txt", vs, vs_size);
	File::Read("../data/shader/dx/first_ps.txt", ps, ps_size);
	int sh = m_render_system->CreateShader(vs, ps);
	m_render_system->SelectShader(sh);

	// vertex buffer
	struct Vec2 { float x, y; };
	struct Vertex { Vec3 p; Vec2 t; };
	float v = 1.0f;
	float s[5] = { 0.0f, 0.25f, 0.5f, 0.75f, 1.0f };
	float t[3] = { 0.0f, 0.5f, 1.0f};
	Vertex vertices[] = {
		// front
		{ Vec3(-v, v,-v), { 0.0f, 0.0f } }, // 0, 1, 2, 2, 3, 0,
		{ Vec3( v, v,-v), { 1.0f, 0.0f } },
		{ Vec3( v,-v,-v), { 1.0f, 1.0f } },
		{ Vec3(-v,-v,-v), { 0.0f, 1.0f } },

		// right
		{ Vec3( v, v,-v), { s[1], t[1] } }, // 4, 5, 6, 6, 7, 4
		{ Vec3( v, v, v), { s[2], t[1] }},
		{ Vec3( v,-v, v), { s[2], t[2] }},
		{ Vec3( v,-v,-v), { s[1], t[2] }},

		// back
		{ Vec3( v, v, v), { s[2], t[1] }}, // 8, 9, 10, 10, 11, 8
		{ Vec3(-v, v, v), { s[3], t[1] }},
		{ Vec3(-v,-v, v), { s[3], t[2] }},
		{ Vec3( v,-v, v), { s[2], t[2] }},

		// left
		{ Vec3(-v, v, v), { s[3], t[1] }}, // 12, 13, 15, 15, 16, 12
		{ Vec3(-v, v,-v), { s[4], t[1] }},
		{ Vec3(-v,-v,-v), { s[4], t[2] }},
		{ Vec3(-v,-v, v), { s[3], t[2] }},

		// top
		{ Vec3(-v, v, v), { s[2], t[0] }}, // 16, 17, 18, 18, 19, 16
		{ Vec3( v, v, v), { s[2], t[1] }},
		{ Vec3( v, v,-v), { s[1], t[1] }},
		{ Vec3(-v, v,-v), { s[1], t[0] }},

		// bottom
		{ Vec3(-v,-v,-v), { 0.0f, 1.0f }}, // 20, 21, 22, 22, 23, 20
		{ Vec3( v,-v,-v), { 0.0f, 1.0f }},
		{ Vec3( v,-v, v), { 0.0f, 1.0f }},
		{ Vec3(-v,-v, v), { 0.0f, 1.0f } },
	};
	int vb = m_render_system->CreateVertexBuffer(ACCESS_NONE, vertices, sizeof(vertices));
	m_render_system->SelectVertexBuffer(vb);

	// index buffer
	short* indices = new short[36];
	for(int i = 0, index = 0; i < 6; i++) {
		indices[index++] = i * 4 + 0;
		indices[index++] = i * 4 + 1;
		indices[index++] = i * 4 + 2;
		indices[index++] = i * 4 + 2;
		indices[index++] = i * 4 + 3;
		indices[index++] = i * 4 + 0;
	};
	int ib = m_render_system->CreateIndexBuffer(indices, sizeof(short), 36);
	delete[] indices;
	m_render_system->SelectIndexBuffer(ib);

	// vertex format i.e input layout
	FormatDesc desc[] = {
		{ 0, ATTRIB_POSITION, FORMAT_FLOAT, 3 },
		{ 0, ATTRIB_TEXCOORD, FORMAT_FLOAT, 2 },
	};
	int vf = m_render_system->CreateVertexFormat(desc, 2, sh);
	m_render_system->SelectVertexFormat(vf);

	// fill shaders constantbuffer 
	// note: shader has to be selected before setting shader constants
	struct {
		Mat4 proj;
		Mat4 view;
	} frame;
	Mat4::Translation(frame.view, Vec3(0.0f, 0.0f, 10.0f));
	Mat4::Perspective(frame.proj, 45.0f * 3.141592f / 180.0f, (float)width / (float)height, 0.5f, 1000.0f); 
	m_render_system->SetShaderConstantMatrix("projection", frame.proj);
	m_render_system->SetShaderConstantMatrix("view", frame.view);

	// texture
	FILE* file = fopen("../data/texture/crate.png", "rb");
	int components = 0;
	unsigned char* buffer = stbi_load_from_file(file, &width, &height, &components, 4);
	fclose(file);
	int tx = m_render_system->CreateTexture(buffer, width, height, components - 1);
	stbi_image_free(buffer);
	m_render_system->SelectTexture(tx);

	// sampler state
	int sa = m_render_system->CreateSamplerState(ADDRESS_CLAMP, ADDRESS_CLAMP, FILTER_LINEAR);
	m_render_system->SelectSamplerState(sa);

	// we need to apply all changes (settings)
	m_render_system->Apply();
	
	return true;
};

void Engine::Cleanup() {
	if(m_input_system) {
		delete m_input_system;
		m_input_system = nullptr;
	};
	if(m_render_system) {
		delete m_render_system;	
		m_render_system = nullptr;
	};
};

static Mat4 world;
static float rot;
bool Engine::Update(float deltatime) {
	rot += deltatime;
	Vec3 axis(1.0f, 1.0f, 0.0f);
	axis.Normalize();

	Mat4::Rotation(world, axis, rot);

	m_render_system->Clear();

	m_render_system->SetShaderConstantMatrix("world", world);
	m_render_system->Apply();
	m_render_system->DrawIndexed(PRIMITIVE_TRIANGLES, 0, 36);

	m_render_system->Present();
	return true;
};

void Engine::OnMouseMove(int x, int y) {
	m_input_system->OnMouseMove(x, y);
};

void Engine::OnMouseButton(int button, bool state) {
	m_input_system->OnMouseButton(button, state);
};

void Engine::OnKeyboard(int key, bool state) {
	m_input_system->OnKeyboard(key, state);
};

void Engine::OnFocus(bool state) {
};

void Engine::OnQuit() {
};
